package TelescopicConstructor;

public class TelescopicConstructor {
    public static void main(String[] args) {
        System.out.println("Basic NormalConstruction.NormalConstructorCPU"+new TelescopicConstructorCPU("AMD","MSI","1TBHDD","4GB","T20Game"));
        System.out.println("Gaming NormalConstruction.NormalConstructorCPU"+new TelescopicConstructorCPU("AMD RYZEN","MSI R4580","8gb","1TBSSD","T20RYZEN","NVIDIA2010Ti","","","Full Get Go"));
    }
}

class TelescopicConstructorCPU {

    //Compulsory
    private String processor;
    private String motherBoard;
    private String hardDisk;
    private String ram;
    private String casing;

    //optional
    private String videoCard;
    private String cdRom;
    private String coolingMechanism;
    private String rgbLights;
    private String fans;

    public TelescopicConstructorCPU(String processor, String motherBoard, String hardDisk, String ram, String casing) {
        this(processor,motherBoard,hardDisk,ram,casing,"");
    }

    public TelescopicConstructorCPU(String processor, String motherBoard, String hardDisk, String ram, String casing, String videoCard) {
        this(processor,motherBoard,hardDisk,ram,casing,videoCard,"");
    }

    public TelescopicConstructorCPU(String processor, String motherBoard, String hardDisk, String ram, String casing, String videoCard, String cdRom) {
        this(processor,motherBoard,hardDisk,ram,casing,videoCard,cdRom,"");
    }


    public TelescopicConstructorCPU(String processor, String motherBoard, String hardDisk, String ram, String casing, String videoCard, String cdRom, String coolingMechanism) {
        this(processor,motherBoard,hardDisk,ram,casing,videoCard,cdRom,coolingMechanism,"");
    }

    public TelescopicConstructorCPU(String processor, String motherBoard, String hardDisk, String ram, String casing, String videoCard, String cdRom, String coolingMechanism, String rgbLights) {
        this(processor,motherBoard,hardDisk,ram,casing,videoCard,cdRom,coolingMechanism,rgbLights,"");
    }


    public TelescopicConstructorCPU(String processor, String motherBoard, String hardDisk, String ram, String casing, String videoCard, String cdRom, String coolingMechanism, String rgbLights, String fans) {
        this.processor = processor;
        this.motherBoard = motherBoard;
        this.hardDisk = hardDisk;
        this.ram = ram;
        this.casing = casing;
        this.videoCard = videoCard;
        this.cdRom = cdRom;
        this.coolingMechanism = coolingMechanism;
        this.rgbLights = rgbLights;
        this.fans = fans;
    }

    @Override
    public String toString() {
        return "TelescopicConstructor.TelescopicConstructorCPU{" +
                "processor='" + processor + '\'' +
                ", motherBoard='" + motherBoard + '\'' +
                ", hardDisk='" + hardDisk + '\'' +
                ", ram='" + ram + '\'' +
                ", casing='" + casing + '\'' +
                ", videoCard='" + videoCard + '\'' +
                ", cdRom='" + cdRom + '\'' +
                ", coolingMechanism='" + coolingMechanism + '\'' +
                ", rgbLights='" + rgbLights + '\'' +
                ", fans='" + fans + '\'' +
                '}';
    }
}
