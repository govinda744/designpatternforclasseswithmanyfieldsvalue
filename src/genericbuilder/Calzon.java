package genericbuilder;

public class Calzon extends SingleBuilderPizza {

    private final boolean sauceInside;

    public static class Builder extends SingleBuilderPizza.Builder<Builder> {

        private boolean sauceInside = false;

        public Builder(boolean sauceInside) {
            this.sauceInside = sauceInside;
        }
        @Override
        public Calzon build() {
            return new Calzon(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
    Calzon(Builder builder) {
        super(builder);
        this.sauceInside = builder.sauceInside;
    }

    @Override
    public String toString() {
        return "Calzon{" +
                "sauceInside=" + sauceInside +
                ", toppings=" + toppings +
                '}';
    }
}
