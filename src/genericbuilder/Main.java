package genericbuilder;

public class Main {

    public static void main(String[] args) {
        NyPizza nyPizza = new NyPizza.Builder(NyPizza.Size.SMALL).addToppings(SingleBuilderPizza.Toppings.SAUSAGE)
                .addToppings(SingleBuilderPizza.Toppings.ONION).build();

        Calzon calzon = new Calzon.Builder(true).addToppings(SingleBuilderPizza.Toppings.MUSHROOM)
                .addToppings(SingleBuilderPizza.Toppings.PEPPER).build();

        System.out.println("New York pizza: "+ nyPizza);
        System.out.println("Calzon pizza: "+calzon);
    }
}
