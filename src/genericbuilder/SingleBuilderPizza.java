package genericbuilder;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

public abstract class SingleBuilderPizza {
    public enum Toppings { HAM, MUSHROOM, ONION, PEPPER, SAUSAGE}

    final Set<Toppings> toppings;

    abstract static class Builder<T extends Builder<T>> {
        EnumSet<Toppings> builderToppings = EnumSet.noneOf(Toppings.class);

        public T addToppings(Toppings topping) {
            builderToppings.add(Objects.requireNonNull(topping));
            return self();
        }

        abstract SingleBuilderPizza build();

        protected abstract T self();
    }

    SingleBuilderPizza(Builder<?> builder) {
        toppings = builder.builderToppings.clone();
    }
}
