package NormalConstruction;

public class NormalConstructor {

    public static void main(String[] args) {
        System.out.println("Basic NormalConstruction.NormalConstructorCPU"+new NormalConstructorCPU("AMD","MSI","1TBHDD","4GB","T20Game","","","","",""));
        System.out.println("Gaming NormalConstruction.NormalConstructorCPU"+new NormalConstructorCPU("AMD RYZEN","MSI R4580","8gb","1TBSSD","T20RYZEN","NVIDIA2010Ti","","WaterColling","Full Get Go","All Surround Peak Fun"));
    }

}

class NormalConstructorCPU {
    //compulsory
    private String processor;
    private String motherBoard;
    private String hardDisk;
    private String ram;
    private String casing;

    //optional
    private String videoCard;
    private String cdRom;
    private String coolingMechanism;
    private String rgbLights;
    private String fans;

    public NormalConstructorCPU(String processor, String motherBoard, String hardDisk, String ram, String casing, String videoCard, String cdRom, String coolingMechanism, String rgbLights, String fans) {
        this.processor = processor;
        this.motherBoard = motherBoard;
        this.hardDisk = hardDisk;
        this.ram = ram;
        this.casing = casing;
        this.videoCard = videoCard;
        this.cdRom = cdRom;
        this.coolingMechanism = coolingMechanism;
        this.rgbLights = rgbLights;
        this.fans = fans;
    }

    @Override
    public String toString() {
        return "NormalConstruction.NormalConstructorCPU{" +
                "processor='" + getProcessor() + '\'' +
                ", motherBoard='" + getMotherBoard() + '\'' +
                ", hardDisk='" + getHardDisk() + '\'' +
                ", ram='" + getRam() + '\'' +
                ", casing='" + getCasing() + '\'' +
                ", videoCard='" + getVideoCard() + '\'' +
                ", cdRom='" + getCdRom() + '\'' +
                ", coolingMechanism='" + getCoolingMechanism() + '\'' +
                ", rgbLights='" + getRgbLights() + '\'' +
                ", fans='" + getFans() + '\'' +
                '}';
    }

    private String getProcessor() {
        return processor;
    }

    private String getMotherBoard() {
        return motherBoard;
    }

    private String getHardDisk() {
        return hardDisk;
    }

    private String getRam() {
        return ram;
    }

    private String getCasing() {
        return casing;
    }

    private String getVideoCard() {
        return videoCard;
    }

    private String getCdRom() {
        return cdRom;
    }

    private String getCoolingMechanism() {
        return coolingMechanism;
    }

    private String getRgbLights() {
        return rgbLights;
    }

    private String getFans() {
        return fans;
    }
}