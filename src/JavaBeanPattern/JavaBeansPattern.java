package JavaBeanPattern;

public class JavaBeansPattern {
    public static void main(String[] args) {
        JavaBeansPatternCPU cpu = new JavaBeansPatternCPU();
        cpu.setProcessor("AMD");
        cpu.setMotherBoard("MSI");
        cpu.setHardDisk("1TBHDD");
        cpu.setRam("4GB");
        cpu.setCasing("T20");

        JavaBeansPatternCPU cpu1 = new JavaBeansPatternCPU();
        cpu1.setProcessor("AMD RYZEN");
        cpu1.setMotherBoard("MSI R4580");
        cpu1.setHardDisk("1TBSSD");
        cpu1.setRam("8GB");
        cpu1.setCasing("T20RYZEN");
        cpu1.setVideoCard("NVIDEA2010Ti");
        cpu1.setCoolingMechanism("WaterColling");

        System.out.println(cpu);
        System.out.println(cpu1);
    }
}

class JavaBeansPatternCPU {
    //compulsory
    private String processor = "Not Assigned";
    private String motherBoard = "Not Assigned";
    private String hardDisk = "Not Assigned";
    private String ram = "Not Assigned";
    private String casing = "Not Assigned";

    //optional
    private String videoCard = "";
    private String cdRom = "";
    private String coolingMechanism = "";
    private String rgbLights = "";
    private String fans = "";

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public void setMotherBoard(String motherBoard) {
        this.motherBoard = motherBoard;
    }

    public void setHardDisk(String hardDisk) {
        this.hardDisk = hardDisk;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public void setCasing(String casing) {
        this.casing = casing;
    }

    public void setVideoCard(String videoCard) {
        this.videoCard = videoCard;
    }

    public void setCdRom(String cdRom) {
        this.cdRom = cdRom;
    }

    public void setCoolingMechanism(String coolingMechanism) {
        this.coolingMechanism = coolingMechanism;
    }

    public void setRgbLights(String rgbLights) {
        this.rgbLights = rgbLights;
    }

    public void setFans(String fans) {
        this.fans = fans;
    }

    @Override
    public String toString() {
        return "JavaBeanPattern.JavaBeansPatternCPU{" +
                "processor='" + processor + '\'' +
                ", motherBoard='" + motherBoard + '\'' +
                ", hardDisk='" + hardDisk + '\'' +
                ", ram='" + ram + '\'' +
                ", casing='" + casing + '\'' +
                ", videoCard='" + videoCard + '\'' +
                ", cdRom='" + cdRom + '\'' +
                ", coolingMechanism='" + coolingMechanism + '\'' +
                ", rgbLights='" + rgbLights + '\'' +
                ", fans='" + fans + '\'' +
                '}';
    }
}
