package BuilderPattern;

public class BuilderPattern {
    public static void main(String[] args) {
        System.out.println(new BuilderPatternCPU.Builder("AMD","MSI","1TBHDD","4GB","T20").build());
        System.out.println(new BuilderPatternCPU.Builder("AMD RYZEN","MSI4580","1TBSSD","8GB","T20Ryzen")
        .rgbLights("Full On").coolingMechanism("WaterBased").videoCard("NVIDIAGTX2010Ti").build());
    }
}

class BuilderPatternCPU {
    private String processor;
    private String motherBoard;
    private String hardDisk;
    private String ram;
    private String casing;
    private String videoCard;
    private String cdRom;
    private String coolingMechanism;
    private String rgbLights;
    private String fans;

    public BuilderPatternCPU(Builder builder) {
        processor = builder.processor;
        motherBoard = builder.motherBoard;
        hardDisk = builder.hardDisk;
        ram = builder.ram;
        casing = builder.casing;
        videoCard = builder.videoCard;
        cdRom = builder.cdRom;
        coolingMechanism = builder.coolingMechanism;
        rgbLights = builder.rgbLights;
        fans = builder.fans;

    }

    @Override
    public String toString() {
        return "BuilderPattern.BuilderPatternCPU{" +
                "processor='" + processor + '\'' +
                ", motherBoard='" + motherBoard + '\'' +
                ", hardDisk='" + hardDisk + '\'' +
                ", ram='" + ram + '\'' +
                ", casing='" + casing + '\'' +
                ", videoCard='" + videoCard + '\'' +
                ", cdRom='" + cdRom + '\'' +
                ", coolingMechanism='" + coolingMechanism + '\'' +
                ", rgbLights='" + rgbLights + '\'' +
                ", fans='" + fans + '\'' +
                '}';
    }

    public static class Builder{
        //Compulsory
        private String processor;
        private String motherBoard;
        private String hardDisk;
        private String ram;
        private String casing;

        //optional
        private String videoCard = "";
        private String cdRom = "";
        private String coolingMechanism = "";
        private String rgbLights = "";
        private String fans = "";

        public Builder(String processor, String motherBoard, String hardDisk, String ram, String casing) {
            this.processor = processor;
            this.motherBoard = motherBoard;
            this.hardDisk = hardDisk;
            this.ram = ram;
            this.casing = casing;
        }

        public Builder videoCard(String val) {
            this.videoCard = val;
            return this;
        }

        public Builder cdRom(String val) {
            this.cdRom = val;
            return this;
        }

        public Builder coolingMechanism (String val) {
            this.coolingMechanism = val;
            return this;
        }

        public Builder rgbLights (String val) {
            this.rgbLights = val;
            return this;
        }

        public Builder fans (String val) {
            this.fans = val;
            return this;
        }

        public BuilderPatternCPU build() {
            return new BuilderPatternCPU(this);
        }
    }
}
